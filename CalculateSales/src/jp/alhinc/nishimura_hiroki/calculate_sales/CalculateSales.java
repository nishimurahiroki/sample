package jp.alhinc.nishimura_hiroki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<String, String>();
		Map<String, Long> map1 = new HashMap<String, Long>();
		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] data = line.split(",");
				if ((!data[0].matches("[0-9]{3}")) || (data.length != 2)) {
					// 支店定義ファイルのフォーマットが不正です
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				map.put(data[0], data[1]);
				map1.put(data[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("支店定義ファイルが存在しません");
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
			try {
				File files = new File(args[0]);
				String[] dr = files.list();
				long i = 1;
				for (String file : dr) {
					if (file.matches("^\\d{8}.rcd$")) {
						try {
							String filename = file;
							String fl[] = filename.split("\\.");
							long number = Integer.parseInt(fl[0]);
							if (number == i) {
								i++;
							} else {
								System.out.println("売上ファイル名が連番になっていません");
								return;
							}
							File rcdfile = new File(args[0], file);
							FileReader fr = new FileReader(rcdfile);
							br = new BufferedReader(fr);
							String ab = br.readLine();
							long cd = Long.parseLong(br.readLine());
							String check = br.readLine();
							String name = rcdfile.getName();
							if (check != null) {
								// エラー処理ファイルの中身が３行ある
								System.out.println(name + "のフォーマットが不正です");
								return;
							}
							if (map.get(ab) == null) {
								// エラー処理支店コード
								System.out.println(name + "支店コード不正");
								return;
							}

							map1.put(ab, map1.get(ab) + cd);
							Long keta = map1.get(ab);
							System.out.println(ab + "," + map.get(ab) + "," + map1.get(ab));
							if (keta > 9999999999L) {
								System.out.println("合計金額が10桁を超えました");
								return;
							}
						} catch (IOException e) {
							System.out.println("予期せぬエラーが発生しました。");

						}

					}

				}
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("closeできませんでした。");
					}
				}
			}
		}
		File file = new File(args[0], "branch.lst");
		FileReader fr = new FileReader(file);
		br = new BufferedReader(fr);
		String line;
		while ((line = br.readLine()) != null) {
			String[] data = line.split(",");

		 File file1 = new File(args[0],"branch.out");
		 FileWriter fw =new FileWriter(file1,true);
		 BufferedWriter bw = new BufferedWriter(fw);
		String wr = (data[0] + "," +)
		 bw.write(data[0]
		}
		 bw.close();
	}
}
// File file1 = new File(args[0],"branch.out");
// FileWriter fw =new FileWriter(file1,true);
// BufferedWriter bw = new BufferedWriter(fw);
// bw.write(ef+"\r\n");

// bw.close();
